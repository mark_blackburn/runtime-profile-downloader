﻿
namespace OrpyxRuntimeProfileDownloader
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.shoePodComboBox = new System.Windows.Forms.ComboBox();
            this.downloadButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.clearProfileButton = new System.Windows.Forms.Button();
            this.setShoeOffPeriodButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.shoeOffPeriodTextBox = new System.Windows.Forms.TextBox();
            this.getShoeOffPeriodButton = new System.Windows.Forms.Button();
            this.serialNumberLabel = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // shoePodComboBox
            // 
            this.shoePodComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.shoePodComboBox.FormattingEnabled = true;
            this.shoePodComboBox.Location = new System.Drawing.Point(38, 54);
            this.shoePodComboBox.Name = "shoePodComboBox";
            this.shoePodComboBox.Size = new System.Drawing.Size(121, 21);
            this.shoePodComboBox.TabIndex = 0;
            this.shoePodComboBox.DropDown += new System.EventHandler(this.shoePodCombo_DropDown);
            this.shoePodComboBox.SelectedIndexChanged += new System.EventHandler(this.shoePodCombo_SelectedIndexChanged);
            // 
            // downloadButton
            // 
            this.downloadButton.Location = new System.Drawing.Point(38, 91);
            this.downloadButton.Name = "downloadButton";
            this.downloadButton.Size = new System.Drawing.Size(121, 23);
            this.downloadButton.TabIndex = 1;
            this.downloadButton.Text = "Download";
            this.downloadButton.UseVisualStyleBackColor = true;
            this.downloadButton.Click += new System.EventHandler(this.downloadButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(38, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(250, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Orpyx Runtime Profile Downloader";
            // 
            // clearProfileButton
            // 
            this.clearProfileButton.Location = new System.Drawing.Point(38, 131);
            this.clearProfileButton.Name = "clearProfileButton";
            this.clearProfileButton.Size = new System.Drawing.Size(121, 23);
            this.clearProfileButton.TabIndex = 3;
            this.clearProfileButton.Text = "Clear Profile";
            this.clearProfileButton.UseVisualStyleBackColor = true;
            this.clearProfileButton.Click += new System.EventHandler(this.clearProfileButton_Click);
            // 
            // setShoeOffPeriodButton
            // 
            this.setShoeOffPeriodButton.Location = new System.Drawing.Point(10, 19);
            this.setShoeOffPeriodButton.Name = "setShoeOffPeriodButton";
            this.setShoeOffPeriodButton.Size = new System.Drawing.Size(41, 23);
            this.setShoeOffPeriodButton.TabIndex = 4;
            this.setShoeOffPeriodButton.Text = "Set";
            this.setShoeOffPeriodButton.UseVisualStyleBackColor = true;
            this.setShoeOffPeriodButton.Click += new System.EventHandler(this.setShoeOffPeriodButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.shoeOffPeriodTextBox);
            this.groupBox1.Controls.Add(this.getShoeOffPeriodButton);
            this.groupBox1.Controls.Add(this.setShoeOffPeriodButton);
            this.groupBox1.Location = new System.Drawing.Point(28, 160);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 50);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Shoe Off Period";
            // 
            // shoeOffPeriodTextBox
            // 
            this.shoeOffPeriodTextBox.Location = new System.Drawing.Point(108, 19);
            this.shoeOffPeriodTextBox.Name = "shoeOffPeriodTextBox";
            this.shoeOffPeriodTextBox.Size = new System.Drawing.Size(39, 20);
            this.shoeOffPeriodTextBox.TabIndex = 6;
            // 
            // getShoeOffPeriodButton
            // 
            this.getShoeOffPeriodButton.Location = new System.Drawing.Point(57, 19);
            this.getShoeOffPeriodButton.Name = "getShoeOffPeriodButton";
            this.getShoeOffPeriodButton.Size = new System.Drawing.Size(41, 23);
            this.getShoeOffPeriodButton.TabIndex = 5;
            this.getShoeOffPeriodButton.Text = "Get";
            this.getShoeOffPeriodButton.UseVisualStyleBackColor = true;
            this.getShoeOffPeriodButton.Click += new System.EventHandler(this.getShoeOffPeriodButton_Click);
            // 
            // serialNumberLabel
            // 
            this.serialNumberLabel.AutoSize = true;
            this.serialNumberLabel.Location = new System.Drawing.Point(183, 57);
            this.serialNumberLabel.Name = "serialNumberLabel";
            this.serialNumberLabel.Size = new System.Drawing.Size(58, 13);
            this.serialNumberLabel.TabIndex = 6;
            this.serialNumberLabel.Text = "                 ";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 254);
            this.Controls.Add(this.serialNumberLabel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.clearProfileButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.downloadButton);
            this.Controls.Add(this.shoePodComboBox);
            this.Name = "MainForm";
            this.Text = "Orpyx Runtime Profile Downloader";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox shoePodComboBox;
        private System.Windows.Forms.Button downloadButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.Button clearProfileButton;
        private System.Windows.Forms.Button setShoeOffPeriodButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button getShoeOffPeriodButton;
        private System.Windows.Forms.TextBox shoeOffPeriodTextBox;
        private System.Windows.Forms.Label serialNumberLabel;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}


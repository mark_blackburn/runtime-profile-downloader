﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.IO.Ports;

using Newtonsoft.Json;

namespace OrpyxRuntimeProfileDownloader
{
    enum UsbCommands : byte
    {
        CMD_SET_SHOEOFF_GRACE_PERIOD = 0xA4,
        CMD_GET_SHOEOFF_GRACE_PERIOD = 0xA5,
        CMD_CLEAR_RUNTIME_PROFILE = 0xD0,
        CMD_GET_RUNTIME_PROFILE = 0xD1,
        CMD_GET_SHOEPOD_NUMBER = 0x24,
    };

    class RuntimeProfileDownloaderManager
    {
        //public struct RuntimeProfileDataPoint
        //{
        //    string timestamp;
        //    int secondsSinceStart;
        //    int batteryMillivolts;
        //    int batteryPercentage;
        //    bool charging;
        //    bool ledState;
        //};

        public bool isRecordBlank(byte[] runtimeDataPointBytes)
        {
            return Enumerable.SequenceEqual(
                runtimeDataPointBytes.ToList().GetRange(0, 8),
                new byte[] { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff }.ToList());
        }

        public object ProcessRuntimeDataPoint(byte[] runtimeDataPointBytes)
        {
            string timestamp = String.Format("{0:D4}-{1:D2}-{2:D2}T{3:D2}:{4:D2}:{5:D2}.{6:D4}Z",
                2000 + runtimeDataPointBytes[7],
                runtimeDataPointBytes[6],
                runtimeDataPointBytes[5],
                runtimeDataPointBytes[4],
                runtimeDataPointBytes[3],
                runtimeDataPointBytes[2],
                BitConverter.ToInt16(runtimeDataPointBytes, 0)
                );
            ulong secondsSinceOn = BitConverter.ToUInt32(runtimeDataPointBytes, 8);
            ushort batteryVoltage = BitConverter.ToUInt16(runtimeDataPointBytes, 12);
            byte batteryPercentage = runtimeDataPointBytes[14];
            bool charging = runtimeDataPointBytes[15] == 1;
            bool ledState = runtimeDataPointBytes[16] == 1;
            ushort shoeOffMinuteCounter = BitConverter.ToUInt16(runtimeDataPointBytes, 18);
            return MakeRuntimeProfileDataPointDictType(
                timestamp,
                secondsSinceOn,
                batteryVoltage,
                batteryPercentage,
                charging,
                ledState,
                shoeOffMinuteCounter);
        }

        public List<object> ProcessRuntimeProfile(byte[] commandResponse)
        {
            List<object> result = new List<object>();

            for (int i = 0; i < commandResponse.Length; i += 32)
            {
                byte[] chunk = commandResponse.ToList().GetRange(i, 32).ToArray();

                // Skip blank records
                if (!isRecordBlank(chunk))
                {
                    result.Add(ProcessRuntimeDataPoint(chunk.ToArray()));
                }
            }

            return result;
        }

        public void DownloadRuntimeProfile(string filename, string portName)
        {
            StreamWriter outputFile = File.CreateText(filename);
            List<Object> runtimeProfile = new List<Object>();

            byte[] commandResponse;

            bool nextRequest = false;
            while (true)
            {
                byte arg = (byte)(nextRequest ? 1 : 0);
                CommandManager.SendCommand(portName, (byte)UsbCommands.CMD_GET_RUNTIME_PROFILE,
                    new byte[] { arg }, out commandResponse);
                if (commandResponse.Length < 32)
                {
                    break;
                }
                runtimeProfile.AddRange(ProcessRuntimeProfile(commandResponse));

                nextRequest = true;
            }

            string jsonResult = JsonConvert.SerializeObject(runtimeProfile, Formatting.Indented);
            outputFile.Write(jsonResult);
            outputFile.Close();
        }


        // TODO: Decide what type to make the output records in the JSON output, a collection/list or a dict 
        static public object MakeRuntimeProfileDataPointListType(
        string timestamp,
            ulong secondsSinceStart,
            ushort batteryMillivolts,
            byte batteryPercentage,
            bool charging,
            bool ledState,
            ushort shoeOffMinuteCounter)
        {
            return new List<object> {
                timestamp,
                secondsSinceStart, 
                batteryMillivolts,
                batteryPercentage, 
                charging, 
                ledState,
                shoeOffMinuteCounter,
            };
        }

        static public object MakeRuntimeProfileDataPointDictType(
        string timestamp,
            ulong secondsSinceStart,
            ushort batteryMillivolts,
            byte batteryPercentage,
            bool charging,
            bool ledState,
            ushort shoeOffMinuteCounter)

        {
            return new {
                timestamp=timestamp,
                secondsSinceStart=secondsSinceStart,
                batteryMillivolts=batteryMillivolts,
                batteryPercentage=batteryPercentage, 
                charging=charging, 
                ledState=ledState,
                shoeOffMinuteCounter= shoeOffMinuteCounter,
            };
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using System.IO.Ports;

using Newtonsoft.Json;

namespace OrpyxRuntimeProfileDownloader
{
    public partial class MainForm : Form
    {

        private string _selectedComPort;
        private Control[] _controlsEnabledForComport;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            _controlsEnabledForComport = new Control[] { downloadButton, clearProfileButton, groupBox1 };

            foreach (Control c in _controlsEnabledForComport)
            {
                c.Enabled = false;
            }
        }

        private void shoePodCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            _selectedComPort = (string)shoePodComboBox.SelectedItem;
            bool enableState = false;

            if (_selectedComPort != "Empty")
            {
                try
                {
                    serialNumberLabel.Text = getSerialNumber().ToString();
                    toolTip1.SetToolTip(serialNumberLabel, null);
                    enableState = true;
                }
                catch (System.TimeoutException ex)
                {
                    serialNumberLabel.Text = "Error";
                    toolTip1.SetToolTip(serialNumberLabel, ex.Message);
                }
                catch (System.UnauthorizedAccessException ex)
                {
                    serialNumberLabel.Text = "Error";
                    toolTip1.SetToolTip(serialNumberLabel, ex.Message);
                }
            }

            foreach (Control c in _controlsEnabledForComport)
            {
                c.Enabled = enableState;
            }

        }

        /* stealed code */

        public List<string> GetAvailableDevices(string comPort, out int shoePodIndex)
        {

            string[] devices = SerialPort.GetPortNames();

            shoePodIndex = 0;
            List<string> usableDevices = new List<string>(devices.Length);
            usableDevices.Add("Empty");
            foreach (string port in devices)
            {
                // If this is our currently selected device then we can't check the COM port because it is in use.
                // However, it should already have been checked so assume it is good.
                if (port.Equals(comPort))
                {
                    usableDevices.Add(port);
                    shoePodIndex = usableDevices.Count - 1;
                }
                else if (!usableDevices.Contains(port)) // Can have duplicates.
                {
                    // Should validate this COM port connects to a Edson pod.
                    usableDevices.Add(port);
                }
            }

            return usableDevices;
        }

        private void shoePodCombo_DropDown(object sender, EventArgs e)
        {
            int shoePodIndex;
            string currentDevice = (string)shoePodComboBox.SelectedItem;
            List<string> devices = GetAvailableDevices(currentDevice, out shoePodIndex);
            shoePodComboBox.DataSource = devices;
            shoePodComboBox.SelectedIndex = shoePodIndex;
        }

        private uint getSerialNumber()
        {
            byte[] response;
            CommandManager.SendCommand(_selectedComPort, (byte)UsbCommands.CMD_GET_SHOEPOD_NUMBER, null, out response, 100);
            return BitConverter.ToUInt32(response, 0);
        }

        private void downloadButton_Click(object sender, EventArgs e)
        {
            uint serialNumber = getSerialNumber();
            DateTime dateTime = DateTime.Now;

            string filename = String.Format("{0,8:00000000}_{1,4:0000}{2,2:00}{3,2:00}_{4,2:00}{5,2:00}.json",
                serialNumber,
                dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute);
            saveFileDialog.FileName = filename;

            //saveFileDialog.FileName = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\runtimeProfile.json";
            saveFileDialog.Filter = "JSON Files|*.json";

            DialogResult result = saveFileDialog.ShowDialog();
            //DialogResult result = DialogResult.OK;

            if (result == DialogResult.OK)
            {
                filename = saveFileDialog.FileName;

                RuntimeProfileDownloaderManager downloadManager = new RuntimeProfileDownloaderManager();
                downloadButton.Enabled = false;
                try
                {
                    downloadManager.DownloadRuntimeProfile(filename, _selectedComPort);
                }
                catch (System.TimeoutException ex)
                {
                    MessageBox.Show(ex.Message, "Download Profile");
                }
                finally
                {
                    downloadButton.Enabled = true;
                }
            }
        }

        private void clearProfileButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Delete all runtime profile data?", "Clear Profile", MessageBoxButtons.OKCancel) ==
                DialogResult.OK)
            {
                byte[] response;
                CommandManager.SendCommand(_selectedComPort, (byte)UsbCommands.CMD_CLEAR_RUNTIME_PROFILE, null, out response, 1000);
            }
        }

        private void getShoeOffPeriodButton_Click(object sender, EventArgs e)
        {
            byte[] response;
            CommandManager.SendCommand(_selectedComPort, (byte)UsbCommands.CMD_GET_SHOEOFF_GRACE_PERIOD, null, out response, 100);
            shoeOffPeriodTextBox.Text = String.Format("{0}", response[0]);
        }

        private void setShoeOffPeriodButton_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] response;
                byte gracePeriod = Byte.Parse(shoeOffPeriodTextBox.Text);
                CommandManager.SendCommand(_selectedComPort, (byte)UsbCommands.CMD_SET_SHOEOFF_GRACE_PERIOD, new byte[] { gracePeriod }, out response, 100);
            }
            catch (Exception ex)
            {
                if (ex is FormatException || ex is OverflowException)
                {
                    MessageBox.Show("Invalid shoe off period", "Set Shoe Off Period");
                    return;
                }

                throw;
            }
        }
    }
}

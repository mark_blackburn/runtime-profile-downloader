﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.IO.Ports;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace OrpyxRuntimeProfileDownloader
{
    class CommandManager
    {
        const int DEFAULT_TIMEOUT = 100;
        static byte[] STX = { 0x55, 0xAA, 0x00, 0xFF };
        static byte ETX = 0xF0;

        static void WaitForStx(SerialPort serialPort, int timeout=DEFAULT_TIMEOUT)
        {
            List<byte> stxBuffer = new List<byte>();

            while (true)
            {
                byte[] buf = new byte[1];
                serialPort.ReadTimeout = timeout;
                serialPort.Read(buf, 0, 1);
                //int readResult = serialPort.ReadByte();
                //if (readResult == -1)
                //{
                //    continue;
                //}
                //stxBuffer.Add((byte)readResult);
                stxBuffer.Add(buf[0]);

                while (stxBuffer.Count > 4)
                {
                    stxBuffer.RemoveAt(0);
                }
                if (Enumerable.SequenceEqual(stxBuffer.ToArray(), STX))
                {
                    return;
                }
            }

        }

        static public void SendCommand(string portName, byte command, byte[] args, out byte[] result, int timeout=DEFAULT_TIMEOUT)
        {
            List<byte> buf = new List<byte>();

            buf.AddRange(STX);

            ushort commandLength = (ushort)((args == null ? 0 : args.Length) + 1);
            buf.AddRange(BitConverter.GetBytes(commandLength));

            buf.Add(command);

            if (args != null)
            {
                buf.AddRange(args);
            }

            buf.Add(ETX);

            SerialPort serialPort = new SerialPort(portName);
            serialPort.Open();
            serialPort.RtsEnable = true;
            serialPort.DtrEnable = true;

            serialPort.BaudRate = 9600;
            serialPort.DataBits = 8;
            serialPort.Handshake = Handshake.None;
            serialPort.StopBits = StopBits.One;
            serialPort.Parity = Parity.None;

            serialPort.Write(buf.ToArray(), 0, buf.Count);

            WaitForStx(serialPort, timeout);

            byte[] responseSizeBuf = new byte[2];
            serialPort.Read(responseSizeBuf, 0, 2);
            ushort responseSize = BitConverter.ToUInt16(responseSizeBuf, 0);

            // Read the returned command
            byte[] commandBuf = new byte[1];
            serialPort.Read(commandBuf, 0, 1);
            if (commandBuf[0] != command)
            {
                // TODO: clear out read buffer?
                throw new Exception("Target responsed with wrong command");
            }

            result = new byte[responseSize - 1];
            serialPort.Read(result, 0, responseSize - 1);

            byte[] etxBuf = new byte[1];
            serialPort.Read(etxBuf, 0, 1);
            serialPort.Close();

            if (etxBuf[0] != ETX)
            {
                throw new Exception("End of command response is not ETX");
            }

        }
    }
}
